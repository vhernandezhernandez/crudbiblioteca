<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Listado de Libros</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<?php
  $servidorbd = "localhost";
  $nombrebd = "prueba";
  $usuariobd= "programador";
  $contraseniabd = "12345";

  $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
  or die('No se ha podido conectar: ' . pg_last_error());

  $query = 'select isbn, titulo_libro from biblioteca.libro';

  $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
?>

<body>
<table>
  <caption>Listado de Libros</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>ISBN</th>
      <th>Titulo de Libro</th>
      <th>Opción</th>
    </tr>
  </thead>
  <tbody>

<?php
  $contador = 1;
  while ($tupla = pg_fetch_array($resultado, null, PGSQL_ASSOC)) {
    $isbn = $tupla['isbn'];
?>
    <tr>
      <td>
        <?php echo $contador++; ?>
      </td>
<?php
    foreach ($tupla as $atributo) {
?>
      <td><?php echo trim($atributo); ?></td>
<?php
    }
?>
      <td>
        <a href="confirmar-libro.php?isbn=<?php echo $isbn; ?>">Eliminar Libro</a>
      </td>
    </tr>
<?php
  }

  pg_free_result($result);
  pg_close($dbconn);
?>

  </tbody>
</table>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>
